import asyncio
from autobahn.asyncio.websocket import WebSocketClientProtocol, WebSocketClientFactory

import stomper
import uuid
import logging

# sender.py
DESTINATION = "/topic/inbox"
stomper.utils.log_init(logging.DEBUG)


class MyClientProtocol(WebSocketClientProtocol, stomper.Engine):

    def __init__(self, username='DUMMY_USER', password='DUMMY_PASS'):
        stomper.Engine.__init__(self)
        WebSocketClientProtocol.__init__(self)

        self.username = username
        self.password = password
        self.senderID = str(uuid.uuid4())
        self.log = logging.getLogger("stomperCliente")

    # stomper
    def connected(self, msg):
        print("connected - Stomper connection open")

        # Once I've connected I want to subscribe to my the message queue.
        stomper.Engine.connected(self, msg)

        self.log.info("senderID:%s Connected: session %s." % (self.senderID, msg['headers']['session']))

        def hello():
            self.log.info("senderID:%s Saying hello (%d)." % (self.senderID, 1))

            f = stomper.Frame()
            f.unpack(stomper.send(DESTINATION, '(%d) hello there from senderID:<%s>' % (1, self.senderID)))
            self.sendMessage(f.pack().encode('utf8'))

            self.factory.loop.call_later(2, hello)

        # start sending messages every second ..
        hello()


        f = stomper.Frame()
        f.unpack(stomper.subscribe(DESTINATION))
        return f.pack()

    # stomper
    def ack(self, msg):

        # Processes the received message. I don't need to generate an ack message.
        self.log.info("senderID:%s Received: %s " % (self.senderID, msg['body']))
        return stomper.NO_REPONSE_NEEDED

    # websocket
    def onOpen(self):
        print("onOpen - WebSocket connection open")
        cmd = stomper.connect(self.username, self.password, 'localhost')
        self.sendMessage(cmd.encode('utf8'))

    # websocket
    def onMessage(self, payload, isBinary):
        if isBinary:
            print("onMessage - Binary payload message")

        else:
            print("onMessage - Text payload message", payload.decode('utf8'))
            print()
            msg = stomper.unpack_frame(payload.decode('utf8'))
            returned = self.react(msg)

            # print( "sender returned: <%s>" % returned)

            if returned:
                self.sendMessage(returned.encode('utf8'))

        print()


# Running Client
if __name__ == '__main__':

    factory = WebSocketClientFactory("ws://localhost:8765")
    factory.protocol = MyClientProtocol

    loop = asyncio.get_event_loop()
    stompClient = loop.create_connection(factory, 'localhost', 8765)
    loop.run_until_complete(stompClient)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        print()
        print('closind cliente ....')
        print()
        stompClient.close()
        loop.close()
