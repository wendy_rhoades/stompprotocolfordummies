import asyncio

from autobahn.asyncio.websocket import WebSocketServerProtocol, WebSocketServerFactory

import stomper
import uuid
import logging


# receiver.py
DESTINATION = "/topic/inbox"
stomper.utils.log_init(logging.DEBUG)


class MyStomp(stomper.Engine):

    def __init__(self, username='', password=''):
        super(MyStomp, self).__init__()
        self.username = username
        self.password = password
        self.receiverId = str(uuid.uuid4())
        self.log = logging.getLogger("stomperServer")

    def connect(self):
        # Generate the STOMP connect command to get a session.
        return stomper.connect(self.username, self.password, 'localhost')

    def connected(self, msg):
        # Once I've connected I want to subscribe to my the message queue.
        super(MyStomp, self).connected(msg)

        self.log.info("receiverId <%s> connected: session %s" % (self.receiverId, msg['headers']['session']))
        f = stomper.Frame()
        f.unpack(stomper.subscribe(DESTINATION))
        return f.pack()

    def ack(self, msg):
        # Process the message and determine what to do with it.
        self.log.info("receiverId <%s> Received: <%s> " % (self.receiverId, msg['body']))

        # return super(MyStomp, self).ack(msg)
        return stomper.NO_REPONSE_NEEDED



# behavior - Simple EchoServer protocol class
class MyServerProtocol(WebSocketServerProtocol):

    def __init__(self, username='DUMMY_USER', password='DUMMY_PASS'):
        WebSocketServerProtocol.__init__(self)
        self.sm = MyStomp(username, password)

    def onOpen(self):
        print("onOpen: WebSocket connection open.")
        print()
        cmd = self.sm.connect()
        self.sendMessage(cmd.encode('utf8'))

    def onMessage(self, payload, isBinary):
        print("onMessage: Message received")
        if isBinary:
            print("Binary message received bytes:", len(payload))

        else:
            print('Text message received:', payload.decode('utf8'))

            msg = stomper.unpack_frame(payload.decode('utf8'))

            returned = self.sm.react(msg)

            # print( "receiver returned <%s>" % returned)

            if returned:
                self.sendMessage(returned.encode('utf8'))

        print()

    def onClose(self, wasClean, code, reason):
        print("onClose: WebSocket connection closed:", reason)
        print()



# Running a Server
if __name__ == '__main__':

    factory = WebSocketServerFactory("ws://localhost:8765")
    factory.protocol = MyServerProtocol

    loop = asyncio.get_event_loop()
    stompServer = loop.create_server(factory, 'localhost', 8765)
    server = loop.run_until_complete(stompServer)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        print()
        print('closind server ....')
        print()
        server.close()
        loop.close()
