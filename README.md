# stompSuscriptions

## Que es STOMP

[STOMP](https://stomp.github.io/) es un protocolo de texto adoptado ampliamente por la industria para el intercambio de msg entre clientes y servidores que lo implementen. Por ejemplo estos servidores o brokers ActiveMQ, Spring or RabbitMQ enntre otros mas tienen soporte para STOMP.

Como explica [Andres Guzmán en su canal](https://www.youtube.com/watch?v=zKkPrwpmesM) STOMP define la forma y metodos que debemos implementar para hacer el intercambio de msg de texto entre el cliente y el bkroker o agente. STOMP *NO es un cliente de red*, se monta por encima de uno para hacer la conexion implementando las siguientes operaciones para interactuar con su contraparte de tal forma que tanto el cliente como el servidor sin conocerce sepan como procesar los msg.

- SEND
- SUBSCRIBE
- UNSUBSCRIBE
- ACK
- NACK
- BEGIN
- COMMIT
- ABORT
- DISCONNECT


## Librerias

Buscando librerias encontramos dos alternativas relevantes. Ambas implementan el protocolo con la diferencia en que [stomp.py](https://pypi.org/project/stomp.py/) ademas trae un cliente para la conexion de red mientras que [stomper.py](https://pypi.org/project/stomper/) no.

### stomp.py

[Documentacion](http://jasonrbriggs.github.io/stomp.py/index.html)

[Ejemplos muy basico en JAVA y Python](https://github.com/sithu/stomp-client)

[App para consumir informacion del servicio de trenes en Great Britain](https://github.com/openraildata/stomp-client-python) Estoy esperando si es que me crean la cuenta para consumir los msg.

	

### stomper.py
[Stomper provides functions to create and parse STOMP messages in a programmatic fashion.](https://github.com/oisinmulvihill/stomper)

